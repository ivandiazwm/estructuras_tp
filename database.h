#ifndef DATABASE_H
#define DATABASE_H

/*
* Genera un for que recorre los fields de un contacto
*/
#define FOR_FIELD(i) enum ContactField i;for( i = PHONE; i < PERSON_FIELDS; i++)

/*
* Guarda un contacto en la base de datos (7 BSTs)
*/
void database_add(Contact *contact);

/*
* Inicializa la base de datos de contactos
*/
void database_init();

/*
* Borrar un Contacto de la base de datos por su valor en <phone>
*/
int database_delete(int phone);

/*
* Funcion de visita para DFS, imprime el valor de un contacto
*/
void visit_print_contact(void *contact);

/*
* Imprime la lista de contactos ordenada por <field>
*/
void database_print_list(enum ContactField field);

/*
* Edita un Contacto con telefono <phone>.
* Le pone al field <f> el valor <v>
*/
int database_edit(int phone, char *field, char *value);

/*
* Visita todos los contactos en ordenados por <field>
*/
void database_foreach(enum ContactField field, void (*visit)(void*));

/*
* Visita todos los contactos con valor <value> en <field>
*/
void database_foreach_field(char *field, char *value, void (*visit)(void*));

/*
* Devuelve un contacto con minimo valor en <field>.
*/
Contact* database_find_min(char *field);

/*
* Devuelve un contacto con maximo valor en <field>.
*/
Contact* database_find_max(char *field);

/*
* Devuelve el promedio de todos los valores en <field>
*/
float database_average(char *field);

#endif
