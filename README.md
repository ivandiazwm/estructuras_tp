## Estructura de Datos y Algoritmos I: TP Final
------

### Especificación
Se han realizado las siguientes funcionalidades de la especificación del TP:

**Basicas**

- _Interfaz de usuario:_ Un menú con opciones que permitan acceder a la funcionalidad del
programa.

- _Buscar:_ Dado un atributo de un contacto, encontrar otro (o todos) los atributos de ese
contacto.

- _Inserción:_ Insertar un nuevo contacto en nuestra Agenda.

- _Eliminación:_ Eliminar un contacto de nuestra Agenda usando algún atributo específico
que permita identificarlo de manera unívoca.

- _Edición:_ Actualización de un atributo de un contacto dando un atributo específico que
permita identificarlo de manera unívoca.

- _Carga:_ Lectura de contactos desde un archivo externo con un formato que debe ser
especificado en la documentación. El volumen de datos a cargar puede ser variable.

- _Grabación:_ Escritura de la Agenda de Contactos en un archivo externo (debe contener
el mismo formato que la Carga).

**Intermedias**

- _Max (Min):_ Encuentra el máximo (mínimo) de un atributo específico

- _Average:_ Calcula el promedio de un atributo (numérico).

**Avanzadas**

- _Ordenar:_ la lista de Contactos debería poder ordenarse por cualquier atributo. Esto
debería funcionar para todos los atributos, mostrándolos ordenados por el atributo
pedido.


### Estructura de datos usada
Se utiliza Binary Search Tree para implementar un Set, ya que es suficiente para las funcionalidades mencionadas en la especificación.
Hay un Set (BST) por cada atributo que va a tener punteros a contactos ordenados de acuerdo a cada atributo.

La cantidad de atributos es constante y por lo tanto crece en el mismo orden en memoria que con un solo BST.


```
Inserción:
  Worst Case: log(n) - Average Case: log(n)
  Si se usara una HashTable, se puede conseguir en O(1).

Eliminación:
  Worst Case: log(n) - Average Case: log(n)  
  Si se usara una HashTable, se puede conseguir en O(1).

Busqueda:
  Worst Case: n - Average Case: log(n)
  Se tienen que imprimir todos los matches, por lo que siempre va a ser al menos O(n).

Edicion:
  Worst Case: log(n) - Average Case: log(n)
  Si se usara una HashTable, se puede conseguir en O(1).

Carga:
  Worst Case: n log(n) - Average Case: n log(n)
  Se tiene que leer todo el archivo, por lo que siempre va a ser al menos O(n).

Grabación:
  Worst Case: n - Average Case: n
  Se tienen que escribir todos los contactos, por lo que siempre va a ser al menos O(n).

Max/Min:
  Worst Case: log(n) - Average Case: log(n)
  En una HashTable tardaria O(n). Se podria hacer O(1) si no ubiese eliminación.

Average:
  Worst Case: 1 - Average Case: 1
  Se puede actualizar el valor en cada adición/eliminación y tenerlo guardado.

Ordenar:
  Worst Case: n - Average Case: n
  Hay que imprimir toda la lista, entonces es al menos O(n). Con HashTable tardaría O(n log(n)).
```

### Compilación
Basta con ejecutar `make` para compilar el código.

`./main` ejecuta el programa. Se pueden usar los siguientes comandos:

- `help` da informacion sobre los comandos disponibles

- `list` muestra todos los contactos ordenados por telefono

- `listby <field>` mustra todos los contactos ordenados por otro campo

- `add <phone>` agrega un contacto

- `rm <phone>` remueve un contacto

- `edit <phone> <field> <value>` edita un campo de un contacto

- `search <field> <value>` busca la lista de contactos que tienen un valor en un campo

- `average <field_n>` da el promedio de un campo

- `max <field>` busca un máximo de un field

- `min <field>` busca un mínimo de un field

- `load <file>` carga datos de un archivo

- `save <file>` se guardan los datos en un archivo

- `clear` limpia la pantalla

### Input mock
Hay 4 input mocks para usar con el comando `load`: `input_100.txt`, `input_1000.txt`, `input_1000.txt`, `input_1000000.txt`

Se sabe que la carga de datos tarda O(nlog(n)) y como son 7 BST puede tardar 7*n*lg(n), por lo que para el input de 1 millon va a necesitar 113 millones de instrucciones aproximadamente. Para una computadora de 1 MIPS, seria 113 seg.

Para 1 millon de contactos, lo unico que es realmente costoso es la carga. Agregar, eliminar, buscar, listar, min/max, average y guardar tarda significativamente menos.

En memoria, se tienen 153 bytes por la struct `Contact`, y como tenemos 7 bst con punteros a estas estructuras, 4x4x7=112 bytes adicionales por contacto.

El tamaño en memoria crece linealmente O(n).

Para 100 de contactos, consume ~26.5kB
Para 1000 de contactos, consume ~256kB
Para 10000 de contactos, consume ~2.65MB
Para 1000000 de contactos, consume ~265MB
