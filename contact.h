#ifndef PERSON_H
#define PERSON_H

#define MAX_PHONE 9999999
#define MIN_PHONE 0
#define PERSON_FIELDS 7

/*
* ContactField enumera los fields de cada contacto
*/
enum ContactField {
	PHONE,
	NAME,
	LAST_NAME,
	AGE,
	GENDER,
	EMAIL,
	DATE
} contact_cmp_field; // esta variable se usa para saber por que field comparar

/*
* Recibe un string con el nombre del field y lo convierte a <enum ContactField>
*/
enum ContactField get_contact_field(char *f);

/*
* Estructura de Contacto
*/
typedef struct _contact {
	char name[32], lastname[32], email[64], date[16];
	char gender;
	int age;
	int phone;
} Contact;

/*
* Compara dos contactos usando primero por su valor en <contact_cmp_field> y
* luego por su valor en phone
*/
int contact_cmp(void *_p1, void *_p2);

/*
* Imprime un contacto en una sola linea
*/
void contact_print(Contact *p);

/*
* Imprime un contacto en varias lineas
*/
void contact_print_details(Contact *p);

#endif
