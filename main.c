/**
 * TP - Estructuras de Datos y Algoritmos I
 * Agenda de Contactos
 * Ivan Diaz - Legajo: D-3857/1
 * */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bst.h"
#include "contact.h"
#include "database.h"

FILE *fp;

void trash_line() {
	char *trash = NULL;
	size_t trash_len=0;
	getline(&trash, &trash_len, stdin);
}

void handle_contact_list(int read_type) {
	char type[128];
	if(read_type) {
		scanf("%s", type);
		database_print_list(get_contact_field(type));
	} else {
		database_print_list(PHONE);
	}
}

void handle_add_contact() {
	Contact *contact = malloc(sizeof(Contact));

	printf("Nombre (sin espacios en blanco): "); scanf("%s", contact->name); trash_line();
	printf("Apellido (sin espacios en blanco): "); scanf("%s", contact->lastname); trash_line();
	printf("Edad: "); scanf("%d", &contact->age); trash_line();
	printf("Genero (m/f): "); scanf("%c", &contact->gender); trash_line();
	printf("Telefono (7 digitos): "); scanf("%d", &contact->phone); trash_line();
	printf("Email: "); scanf("%s", contact->email); trash_line();
	printf("Fecha de Nacimiento (formato DD/MM/YYYY): "); scanf("%s", contact->date); trash_line();

	printf("Se va a agregar la siguiente contacto:\n");
	contact_print_details(contact);
	printf("Confirmar (y/n)? ");
	char op;
	scanf("%c", &op);
	if(op == 'y') {
		database_add(contact);
	} else {
		free(contact);
	}
}

void handle_delete_contact() {
	int phone;
	scanf("%d", &phone);
	if(database_delete(phone)) {
		printf("Se ha borrado contacto correctamente.\n");
	} else {
		printf("Error: contacto no encontrado.\n");
	}
}

void handle_edit_contact() {
	int phone;
	char f[128], v[128];
	scanf("%d %s %s", &phone, f, v);
	if(database_edit(phone, f, v)) {
		printf("Se ha editado el contacto correctamente.\n");
	} else {
		printf("Error: contacto no encontrado.\n");
	}
}

void handle_load_file() {
  char filename[128]; scanf("%s", filename);
  fp = fopen(filename, "r");

  if(fp == NULL) {
    printf("Invalid file.\n");
    return;
  }

  char name[128], lastname[128], email[128], date[128];
  int age, phone;
  char gender;
  while(fscanf(fp, "%d %s %s %d %c %s %s", &phone, lastname, name, &age, &gender, email, date) == 7) {
    Contact *p = malloc(sizeof(Contact));
    strcpy(p->name, name);
    strcpy(p->lastname, lastname);
    strcpy(p->email, email);
    strcpy(p->date, date);
    p->age = age;
    p->phone = phone;
    p->gender = gender;
    database_add(p);
  }

  fclose(fp);
}

void print_contact_in_file(void *_p) {
  Contact *p = (Contact*)_p;
  fprintf(
    fp, "%d %s %s %d %c %s %s\n",
    p->phone, p->lastname, p->name, p->age, p->gender, p->email, p->date
  );
}

void handle_save_file() {
  char filename[128]; scanf("%s", filename);
  fp = fopen(filename, "w+");

  if(fp == NULL) {
    printf("Invalid file.\n");
    return;
  }
  database_foreach(PHONE, print_contact_in_file);

  fclose(fp);
}

void handle_search_contact() {
  char f[128], v[128];
	scanf("%s %s", f, v);

  database_foreach_field(f, v, visit_print_contact);
}

void handle_find_max() {
	char f[128]; scanf("%s", f);
	Contact *p = database_find_max(f);
	contact_print_details(p);
}

void handle_find_min() {
	char f[128]; scanf("%s", f);
	Contact *p = database_find_min(f);
	contact_print_details(p);
}

void handle_average() {
	char f[128]; scanf("%s", f);
	printf("%s average: %.2f\n", f, database_average(f));
}

void handle_help() {
	printf("*************************************************************************\n");
	printf("<field> puede ser name, lastname, date, email, age, gender, phone\n");
	printf("<field_n> puede ser age, phone\n");
	printf("\n");
	printf("Listar\n");

	printf("  list                          muestra la lista de contactos\n");
	printf("                                ordenado por telefono\n");
	printf("  listby <field>                muestra la lista de contactos\n");
	printf("                                ordenado por <field>\n");

	printf("Buscar\n");
	printf("  search name <name>            busca contactos por nombre\n");
	printf("  search lastname <lastname>    busca contactos por apellido\n");
	printf("  search age <age>              busca contactos por edad\n");
	printf("  search gender <{m,f}>         busca contactos genero m o f\n");
	printf("  search phone <phone>          busca contactos telefono\n");
	printf("  search email <email>          busca contactos email\n");
	printf("  search date <date>            busca contactos por fecha de nacimiento\n");

	printf("Agregar\n");
	printf("  add                           agrega un contacto\n");

	printf("Eliminar\n");
	printf("  rm <phone>                    borrar contacto por email\n");

	printf("Editar\n");
	printf("  edit <phone> <field> <value>  cambia <field> al valor <value>\n");
	printf("                                del contacto con telefono <phone>\n");

	printf("Calcular\n");
	printf("  average <field_n>             calcula el valor promedio de <field_n>\n");
	printf("  max <field>                   busca un contacto con valor más alto en <field>\n");
	printf("  min <field>                   busca un contacto con valor más bajo en <field>\n");

	printf("Cargar\n");
	printf("  load <file>                   carga contactos de archivo\n");

	printf("Grabar\n");
	printf("  save <file>                   guarda contactos en archivo\n");

	printf("Limpiar\n");
	printf("  clear                         limpia la pantalla\n");
	printf("*************************************************************************\n");
}

void listen() {
	char cmd[64];
	printf(">>> ");
	scanf("%s", cmd);
	if(strcmp(cmd, "help") == 0) {
		handle_help();
	} else if(strcmp(cmd, "list") == 0) {
		handle_contact_list(0);
	} else if(strcmp(cmd, "listby") == 0) {
		handle_contact_list(1);
	} else if(strcmp(cmd, "add") == 0) {
		handle_add_contact();
	} else if(strcmp(cmd, "rm") == 0) {
		handle_delete_contact();
	} else if(strcmp(cmd, "edit") == 0) {
		handle_edit_contact();
  } else if(strcmp(cmd, "search") == 0) {
    handle_search_contact();
	} else if(strcmp(cmd, "max") == 0) {
    handle_find_max();
	} else if(strcmp(cmd, "min") == 0) {
    handle_find_min();
	} else if(strcmp(cmd, "average") == 0) {
    handle_average();
  } else if(strcmp(cmd, "load") == 0) {
    handle_load_file();
  } else if(strcmp(cmd, "save") == 0) {
    handle_save_file();
	} else if(strcmp(cmd, "clear") == 0) {
		system("clear");
	} else {
		printf("Invalid command\n");
	}
	trash_line();
}

int main() {
	database_init();

	printf("Agenda de Contactos - (\"help\" para listar comandos)\n");
	printf("****************************************************\n");
	while(1) listen();
	return 0;
}
