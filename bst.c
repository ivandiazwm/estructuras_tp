#include "bst.h"
#include "contact.h"
#include <stdlib.h>
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

int bst_height(BST *root) {
	if(root == NULL) return 0;
	return root->height;
}

BST *bst_right_rotate(BST *z) {
    BST *y = z->left;
    BST *x = y->right;

    y->right = z;
    z->left = x;

		z->height = MAX(bst_height(z->left), bst_height(z->right))+1;
		y->height = MAX(bst_height(y->left), bst_height(y->right))+1;

    return y;
}

BST *bst_left_rotate(BST *z) {
    BST *y = z->right;
    BST *x = y->left;

    y->left = z;
    z->right = x;
		z->height = MAX(bst_height(z->left), bst_height(z->right))+1;
		y->height = MAX(bst_height(y->left), bst_height(y->right))+1;

    return y;
}

int bst_get_balance(BST *root) {
	if(root == NULL) return 0;
	return bst_height(root->left) - bst_height(root->right);
}

BST *bst_add(BST *root, void *key, int (*cmp)(void*, void*)) {
	if(root == NULL) {
		root = malloc(sizeof(BST));
		root->left = NULL;
		root->right = NULL;
		root->key = key;
		root->height = 1;

		return root;
	}

	if(cmp(key, root->key)) {
		root->left = bst_add(root->left, key, cmp);
	} else if(cmp(root->key, key)) {
		root->right = bst_add(root->right, key, cmp);
	}

	root->height = 1 + MAX(bst_height(root->left), bst_height(root->right));

	int balance = bst_get_balance(root);

	if(balance > 1 && cmp(key, root->left->key))
		return bst_right_rotate(root);

	if(balance < -1 && cmp(root->right->key, key))
		return bst_left_rotate(root);

	if(balance > 1 && cmp(root->left->key, key)) {
		root->left = bst_left_rotate(root->left);
		return bst_right_rotate(root);
	}

	if(balance < -1 && cmp(key, root->right->key)) {
		root->right = bst_right_rotate(root->right);
		return bst_left_rotate(root);
	}

	return root;
}

BST *bst_find(BST *root, void *key, int (*cmp)(void*, void*)) {
	if(root == NULL) return NULL;
	if(cmp(key, root->key)) {
		return bst_find(root->left, key, cmp);
	} else if(cmp(root->key, key)) {
		return bst_find(root->right, key, cmp);
	} else {
		return root;
	}
}

BST *bst_get_min(BST *root) {
	if(root == NULL) return NULL;
	if(root->left == NULL && root->right == NULL) {
		return root;
	}

	if(root->left == NULL) {
		return root;
	} else {
		return bst_get_min(root->left);
	}
}

BST *bst_get_max(BST *root) {
	if(root == NULL) return NULL;
	if(root->left == NULL && root->right == NULL) {
		return root;
	}

	if(root->right == NULL) {
		return root;
	} else {
		return bst_get_max(root->right);
	}
}

BST *bst_delete(BST *root, void *key, int (*cmp)(void*, void*)) {
	if(root == NULL) {
		return NULL;
	}

	if(cmp(key, root->key)) {
		root->left = bst_delete(root->left, key, cmp);
	} else if(cmp(root->key, key)) {
		root->right = bst_delete(root->right, key, cmp);
	} else {
		if(root->right == NULL && root->left == NULL) {
			free(root);
			return NULL;
		} else if(root->right == NULL || root->left == NULL) {
			if(root->right != NULL) {
				BST *temp = root;
				root = root->right;
				free(temp);
			} else {
				BST *temp = root;
				root = root->left;
				free(temp);
			}
		} else {
			BST *min_right = bst_get_min(root->right);
			root->key = min_right->key;
			root->right = bst_delete(root->right, min_right->key, cmp);
		}
	}

	if(root == NULL) return root;

	root->height = 1 + MAX(bst_height(root->left), bst_height(root->right));

	int balance = bst_get_balance(root);
  if (balance > 1 && bst_get_balance(root->left) >= 0)
      return bst_right_rotate(root);

  if (balance > 1 && bst_get_balance(root->left) < 0) {
      root->left = bst_left_rotate(root->left);
      return bst_right_rotate(root);
  }

  if (balance < -1 && bst_get_balance(root->right) <= 0)
      return bst_left_rotate(root);

  if (balance < -1 && bst_get_balance(root->right) > 0) {
      root->right = bst_right_rotate(root->right);
      return bst_left_rotate(root);
  }

	return root;
}

void bst_dfs(BST *root, void (*visit)(void*)) {
	if(root == NULL) return;
	bst_dfs(root->left, visit);
	visit(root->key);
	bst_dfs(root->right, visit);
}

void bst_dfs_range(BST *root, void *a, void *b, int (*cmp)(void*, void*), void (*visit)(void*)) {
	if(root == NULL) return;
	if(cmp(root->key, a)) {
		bst_dfs_range(root->right, a, b, cmp, visit);
	} else if(cmp(b, root->key)) {
		bst_dfs_range(root->left, a, b, cmp, visit);
	} else {
		bst_dfs_range(root->left, a, b, cmp, visit);
		visit(root->key);
		bst_dfs_range(root->right, a, b, cmp, visit);
	}
}
