#include <stdlib.h>
#include <string.h>
#include "bst.h"
#include "contact.h"
#include "database.h"

BST* database[PERSON_FIELDS];
float average[PERSON_FIELDS];
int size;

void database_add(Contact *contact) {
	contact_cmp_field = PHONE;
	if(bst_find(database[PHONE], contact, contact_cmp) != NULL) {
		return;
	}

	FOR_FIELD(field) {
		contact_cmp_field = field;
		database[field] = bst_add(database[field], contact, contact_cmp);
	}

	average[PHONE] = (average[PHONE]*size + contact->phone) / (size+1);
	average[AGE] = (average[AGE]*size + contact->age) / (size+1);

	size++;
}

void database_init() {
	FOR_FIELD(field) {
		database[field] = NULL;
		average[field] = 0;
	}
	size=0;

	/* Create a default Contact */
	Contact *p1 = malloc(sizeof(Contact));
	strcpy(p1->name, "Ivan");
	strcpy(p1->lastname, "Diaz");
	p1->age = 84;
	p1->phone = 1111245;
	p1->gender = 'm';
	strcpy(p1->email, "ivan@ivan.com");
	strcpy(p1->date, "01/04/2051");

	/* Create a default Contact */
	Contact *p2 = malloc(sizeof(Contact));
	strcpy(p2->name, "Carlos");
	strcpy(p2->lastname, "Eber");
	p2->age = 11;
	p2->phone = 4628212;
	p2->gender = 'f';
	strcpy(p2->email, "ze@carlos.com");
	strcpy(p2->date, "01/04/2012");

	database_add(p1);
	database_add(p2);
}

int database_delete(int phone) {
	Contact *contact = malloc(sizeof(Contact));
	contact->phone=phone;

	contact_cmp_field = PHONE;
	if(bst_find(database[PHONE], contact, contact_cmp) != NULL) {
		free(contact);
		contact = bst_find(database[PHONE], contact, contact_cmp)->key;

		FOR_FIELD(field) {
			contact_cmp_field = field;
			database[field] = bst_delete(database[field], contact, contact_cmp);
		}

		average[PHONE] = (average[PHONE]*size - contact->phone) / (size-1);
		average[AGE] = (average[AGE]*size - contact->age) / (size-1);
		size--;

		free(contact);

		return 1;
	} else {
		return 0;
	}
}

float database_average(char *field) {
	return average[get_contact_field(field)];
}


void visit_print_contact(void *contact) {
	contact_print((Contact*)contact);
}

void database_foreach(enum ContactField field, void (*visit)(void*)) {
	contact_cmp_field = field;
	bst_dfs(database[field], visit);
}

void database_foreach_field(char *field, char *value, void (*visit)(void*)) {
	Contact *a = malloc(sizeof(Contact));
	Contact *b = malloc(sizeof(Contact));
	a->phone = MIN_PHONE;
	b->phone = MAX_PHONE;

	enum ContactField contact_field = get_contact_field(field);
  switch (contact_field) {
		case NAME:
			strcpy(a->name, value);
			strcpy(b->name, value);
			break;
		case LAST_NAME:
			strcpy(a->lastname, value);
			strcpy(b->lastname, value);
			break;
		case EMAIL:
			strcpy(a->email, value);
			strcpy(b->email, value);
			break;
		case DATE:
			strcpy(a->date, value);
			strcpy(b->date, value);
			break;
		case GENDER:
			a->gender = value[0];
			b->gender = value[0];
			break;
		case AGE:
			a->age = atoi(value);
			b->age = atoi(value);
			break;
		case PHONE:
			a->phone = atoi(value);
			b->phone = atoi(value);
			break;
	}

	contact_cmp_field = contact_field;
	bst_dfs_range(database[contact_field], a, b, contact_cmp, visit);

	free(a);
	free(b);
}

void database_print_list(enum ContactField field) {
	contact_cmp_field = field;
	bst_dfs(database[field], visit_print_contact);
}

int database_edit(int phone, char *field, char *value) {
	Contact *contact = malloc(sizeof(Contact));
	contact->phone=phone;

	contact_cmp_field = PHONE;
	BST *x = bst_find(database[PHONE], contact, contact_cmp);

	if(x == NULL) return 0;
	strcpy(contact->name, ((Contact*)x->key)->name);
	strcpy(contact->lastname, ((Contact*)x->key)->lastname);
	strcpy(contact->email, ((Contact*)x->key)->email);
	strcpy(contact->date, ((Contact*)x->key)->date);
	contact->gender = ((Contact*)x->key)->gender;
	contact->age = ((Contact*)x->key)->age;
	contact->phone = ((Contact*)x->key)->phone;

	database_delete(phone);

	enum ContactField contact_field = get_contact_field(field);
	switch (contact_field) {
		case NAME:
			strcpy(contact->name, value);
			break;
		case LAST_NAME:
			strcpy(contact->lastname, value);
			break;
		case EMAIL:
			strcpy(contact->email, value);
			break;
		case DATE:
			strcpy(contact->date, value);
			break;
			contact->gender = value[0];
		case GENDER:
			break;
		case AGE:
			contact->age = atoi(value);
			break;
		case PHONE:
			contact->phone = atoi(value);
			break;
	}

	database_add(contact);

	return 1;
}

Contact* database_find_min(char *field) {
	enum ContactField contact_field = get_contact_field(field);
	BST* p = bst_get_min(database[contact_field]);
	if(p == NULL) return NULL;
	else return (Contact*)p->key;
}

Contact* database_find_max(char *field) {
	enum ContactField contact_field = get_contact_field(field);
	BST* p = bst_get_max(database[contact_field]);
	if(p == NULL) return NULL;
	else return (Contact*)p->key;
}

/*  DATABASE <- END  */
