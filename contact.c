#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "contact.h"

enum ContactField get_contact_field(char *field) {
	if(strcmp(field, "name") == 0) {
		return NAME;
	} else if(strcmp(field, "lastname") == 0) {
		return LAST_NAME;
	} else if(strcmp(field, "email") == 0) {
		return EMAIL;
	} else if(strcmp(field, "date") == 0) {
		return DATE;
	} else if(strcmp(field, "gender") == 0) {
		return GENDER;
	} else if(strcmp(field, "age") == 0) {
		return AGE;
	} else if(strcmp(field, "phone") == 0) {
		return PHONE;
	}

	return -1;
}

int contact_field_cmp(void *_p1, void *_p2){
	Contact *p1 = ((Contact*)_p1);
	Contact *p2 = ((Contact*)_p2);

	switch (contact_cmp_field) {
		case NAME:
			return strcmp(p1->name, p2->name) < 0;
		case LAST_NAME:
			return strcmp(p1->lastname, p2->lastname) < 0;
		case EMAIL:
			return strcmp(p1->email, p2->email) < 0;
		case DATE:
			return strcmp(p1->date, p2->date) < 0;
		case AGE:
			return p1->age < p2->age;
		case GENDER:
			return p1->gender < p2->gender;
		default:
			return p1->phone < p2->phone;
	}
}

int contact_cmp(void *_p1, void *_p2) {
	Contact *p1 = ((Contact*)_p1);
	Contact *p2 = ((Contact*)_p2);

	int phone_cmp = p1->phone < p2->phone;

	if(!contact_field_cmp(_p1, _p2) && !contact_field_cmp(_p2, _p1)) return phone_cmp;
	else return contact_field_cmp(_p1, _p2);
}


void contact_print(Contact *p) {
	if(p == NULL) return;
	printf(
    "%d %s %s %d %c %s %s\n",
    p->phone, p->lastname, p->name, p->age, p->gender, p->email, p->date
  );
}

void contact_print_details(Contact *p) {
	if(p == NULL) printf("Error: contacto no encontrado\n");
	printf("-----------------------------\n");
	printf("%s, %s - Tel.: %d\n", p->lastname, p->name, p->phone);
	printf("Email: %s\n", p->email);
	printf("%d Years old, %s\n", p->age, (p->gender == 'm') ? "Hombre" : "Mujer");
	printf("Birthday: %s\n", p->date);
	printf("-----------------------------\n");
}
