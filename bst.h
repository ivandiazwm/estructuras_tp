#ifndef BST_H
#define BST_H

/*
* Se define un BST AVL
*/
typedef struct _BST {
	struct _BST *left, *right;
	void *key;
	int height;
} BST;

/*
* Retorna la altura para un dado BST
*/
int bst_height(BST *root);

/*
* Calcula la diferencia entre la altura del subarbol izquierdo y la altura
* del subarbol derecho.
*/
int bst_get_balance(BST *root);

/*
* Rota un arbol AVL a derecha
*/
BST *bst_right_rotate(BST *z);

/*
* Rota un arbol AVL a izquierda
*/
BST *bst_left_rotate(BST *z);

/*
* Agrega un elemento al BST
*/
BST *bst_add(BST *root, void *key, int (*cmp)(void*, void*));

/*
* Busca un elemento que sea igual a <key> en BST
*/
BST *bst_find(BST *root, void *key, int (*cmp)(void*, void*));

/*
* Retorna un menor elemento
*/
BST *bst_get_min(BST *root);

/*
* Retorna un mayor elemento
*/
BST *bst_get_max(BST *root);

/*
* Borra un elemento que sea igual <key> en el BST
*/
BST *bst_delete(BST *root, void *key, int (*cmp)(void*, void*));

/*
* Hace una busqueda en profundidad in-order aplicando la funcion visit
*/
void bst_dfs(BST *root, void (*visit)(void*));

/*
* Aplica la funcion visit a todos los elementos que esten en el rango [a, b]
*/
void bst_dfs_range(BST *root, void *a, void *b, int (*cmp)(void*, void*), void (*visit)(void*));

#endif
